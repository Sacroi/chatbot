import { Configuration, OpenAIApi } from "openai";
// import * as dotenv from 'dotenv';
export default class Bot {
  constructor(name, authority) {
    this.name = name;
    this.authority = authority;
  }
  getNitrate(code_commune){
    const date = new Date();
    let request = fetch('https://hubeau.eaufrance.fr/api/v1/qualite_eau_potable/resultats_dis?code_commune='+code_commune+'&code_parametre=1340');
    request.then((e)=>{
      e.json().then((data)=>{
        this.showMessage(this.name,'Nombre d\'informations associées : ' + data.count + '<br> Resultat du test de nitrate : ' + data.data[0].resultat_alphanumerique + ' ' + data.data[0].libelle_unite ,date);
      });
    });
  }

  getTemperature(code_commune,name,date){
    let request = fetch('https://hubeau.eaufrance.fr/api/v1/temperature/chronique?code_departement=' + code_commune + '&size=20');
    request.then((e)=>{
      e.json().then((data)=>{
        console.log(data.data[0]);
        this.showMessage(name,'Station : ' + data.data[0].libelle_station + '<br> Commune : ' + data.data[0].libelle_commune + '<br> Date : ' + data.data[0].date_mesure_temp + ' ' + data.data[0].heure_mesure_temp + '<br> Température : ' + data.data[0].resultat + ' ' + data.data[0].symbole_unite ,date);
      });
    });
  }

  calculMultiplication(value1,value2,name,date){
    this.showMessage(name,'Le résultat du calcul : ' + value1 + ' x ' + value2 + ' = ' + parseInt(value1, 10) * parseInt(value2, 10),date);
  }

  getDataEnterprise(data,name,date){
    let request = fetch('https://recherche-entreprises.api.gouv.fr/search?q=' + data);
    request.then((e)=>{
      console.log(e);
      e.json().then((data)=>{
        console.log(data);
        this.showMessage(name,'Entreprise : ' + data.results[0].nom_complet + '<br> Adresse : ' + data.results[0].siege.adresse + '<br> Date de création : ' + data.results[0].date_creation + '<br> Catégorie : ' + data.results[0].categorie_entreprise ,date);
      });
    });
  }

  async gptCommand(request,successcallback){// ma clef ne fonctionne pas car elle est déjà bloquée donc à l'aveugle
    const configuration = new Configuration({
      organization: "",
      apiKey: "",
    });
    const openai = new OpenAIApi(configuration);
    const response = await openai.createChatCompletion({"model":"gpt-3.5-turbo","messages": [{"role": "user", "content": request}]});
    successcallback(response);
    return response;
  }
  showMessage(name,data,time){
    let messages = window.localStorage.getItem('messages');
    let parsed = JSON.parse(messages);
    parsed.push({owner:name,data:data,date:time})
    localStorage.setItem("messages", JSON.stringify(parsed));
    let myString = '<div class="media w-50 mb-3"><img src="https://bootstrapious.com/i/snippets/sn-chat/avatar.svg" alt="user" width="50" class="rounded-circle"><p class="text-small mb-0 text-black">' + name + '</p><div class="media-body ml-3"><div class="bg-light rounded py-2 px-3 mb-2"><p class="text-small mb-0 text-black">' + data + '</p></div><p class="small text-muted">' + time.getHours() + ':' + time.getMinutes() + '</p></div></div></div>';
    let myElement = document.createElement('div');
    myElement.innerHTML = myString;
    let myDiv = myElement.firstChild;
    document.getElementById('test').appendChild(myDiv);
  }

  response(message) {
    const date = new Date();
    const command = message.split(" ");
    switch (command[0]) {
      case 'ping':
        if (this.authority === 1) {
          this.showMessage(this.name,"pong",date);
        }
        break;
      case 'help':
        if (this.authority === 0) {
          this.showMessage(this.name,'Liste des commandes : <br> - hello (le bot répond au message)<br> - InfoEntreprise "nom" (ne pas oublier l\'espace entre InfoEntreprise et le nom à  savoir que l\'on peut aussi rechercher par adresse et nom de dirigeants, si vous voulez mettre des espace dans les données il faut mettre %20) <br> - gpt1 question (ne pas oublier l\'espace entre gpt et la question)',date);
        }
        if (this.authority === 1) {
          this.showMessage(this.name,'Liste des commandes : <br> - ping (le bot renvoie pong)<br> - hubeau "ville" (ne pas oublier l\'espace entre hubeau et ville) <br> - gpt2 question (ne pas oublier l\'espace entre gpt et la question)',date);
        }
        if (this.authority === 2) {
          this.showMessage(this.name,'Liste des commandes : <br> - calculmultiplication valeur1 valeur2 (le bot renvoie le résultat et surtout mettre en espace entre les valeurs comme dans l\'exemple)<br> - TempRiver "code postal" (ne pas oublier l\'espace entre TempRiver et code postal et code postal à 2 chiffres) <br> - gpt3 question (ne pas oublier l\'espace entre gpt et la question)',date);
        }
        break;
      case 'hubeau':
        if (this.authority === 1) {
          let request = fetch("https://hubeau.eaufrance.fr/api/v1/qualite_eau_potable/communes_udi?nom_commune="+command[1]);
          request.then((e)=>{
            e.json().then((data)=>{
              this.getNitrate(data.data[0].code_commune);
            });
          });
        }
        break;
      case 'TempRiver':
        if (this.authority === 2) {
          this.getTemperature(command[1],this.name,date);
        }
        break;
      case 'InfoEntreprise':
        if(this.authority === 0){
          this.getDataEnterprise(command[1],this.name,date);
        }
        break;
      case 'hello':
        if (this.authority === 0) {
          this.showMessage(this.name,'Hi you!',date);
        }
        break;
      case 'calculmultiplication':
        if (this.authority === 2) {
          this.calculMultiplication(command[1],command[2],this.name,date);
        }
        break;
      case 'gpt1':
          this.gptCommand(command[1],(e)=>{
            if(!e.error){
              this.showMessage(this.name,e.choices.message.content,date);
            }else{
              this.showMessage(this.name,'Erreur dans la requête (fait à l\'aveugle donc bon il n\'y a pas d\'exemple d\'erreur)',date);
            }
          }).then((response)=>{
            console.log(response);
          });
        break;
      case 'gpt2':
        this.gptCommand(command[1],(e)=>{
          if(!e.error){
            this.showMessage(this.name,e.choices.message.content,date);
          }else{
            this.showMessage(this.name,'Erreur dans la requête (fait à l\'aveugle donc bon il n\'y a pas d\'exemple d\'erreur)',date);
          }
        }).then((response)=>{
          console.log(response);
        });
        break;
      case 'gpt3':
        this.gptCommand(command[1],(e)=>{
          if(!e.error){
            this.showMessage(this.name,e.choices.message.content,date);
          }else{
            this.showMessage(this.name,'Erreur dans la requête (fait à l\'aveugle donc bon il n\'y a pas d\'exemple d\'erreur)',date);
          }
        }).then((response)=>{
          console.log(response);
        });
        break;
      default:
        console.log('pas de requêtes !');
    }
  }
 
}
